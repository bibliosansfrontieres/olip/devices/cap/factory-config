# cap-factory-config

This repository allows BSF to reset a CMAL100 (CAP) device to a fresh state.

* Removes traces from a previous [`olip-deploy`](https://gitlab.com/bibliosansfrontieres/olip/olip-deploy) or [`ideascube-deploy`](https://github.com/bibliosansfrontieres/ideascube-deploy/) run.
* Restore some configuration files so the system is "neutral" regarding this previous ansible run.

## Usage

```shell
wget https://gitlab.com/bibliosansfrontieres/olip/devices/cap/factory-config/-/raw/master/reset.sh
bash reset.sh
```

By default this script will keep our remote access means:

* SSH keys
* tinc VPN (configuration, keys)

If you want to remove that also, use the `rm-bsf-access` argument:

```shell
bash reset.sh rm-bsf-access
```

## How to update the factory-config tarball

Grab a CAP from the stock - litteraly in the "out of the box" state.

Copy the wanted files and directories to your local clone, then `git commit`:

```text
/etc/hosts
/etc/config/
```

Next step is to `git push`. The CI will take care of the rest:

* fix rights and file modes
* generate and publish the tarball

## Vanilla config dumps

The `configs/` directory stores some system information:

* `outofthebox/`: brand new CAP device
* `ideascube/`: after `ideascube-deploy`

These dumps are geenerated as follows:

```shell
dpkg --get-selections  > debconf-packages
dpkg -l                > dpkg-l
ls -lR /etc            > files-etc
apt install debconf-utils                               \
  && debconf-get-selections > debconf-get-selections    \
  && apt purge --autoremove debconf-utils
```

## Hard reset

This script only removes **some** of the changes applied by BSF/OLIP.

In order to get the device back to its complete factory configuration,
a [Hard Reset should be performed](http://ideascube-hardware.doc.bibliosansfrontieres.org/en/administrator/reset).
